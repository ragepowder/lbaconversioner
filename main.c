#include <stdio.h>
#include "conversioner.h"


int main(int argc, char* argv[])
{
    printf("This build has no error checking - it may just fail for whatever reason!\n");
    if (argc == 1)
        help();

    return parse_args(argc, argv); 
}
