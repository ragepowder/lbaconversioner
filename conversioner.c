#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include "conversioner.h"

static struct option long_options[] = {
        {"lba", required_argument, NULL, 'l'},
        {"size", required_argument, NULL, 's'},
        {"format", required_argument, NULL, 'f'},
        {"output", required_argument, NULL, 'o'},
        {"help", no_argument, NULL, 'h'},
        {NULL, 0, NULL, 0}
};

unsigned long long lba_to_gb(unsigned long long lba) { return LBA_TO_GB(lba); }
unsigned long long lba_to_mb(unsigned long long lba) { return LBA_TO_MB(lba); }
unsigned long long lba_to_kb(unsigned long long lba) { return LBA_TO_KB(lba); }
unsigned long long lba_to_bt(unsigned long long lba) { return LBA_TO_BT(lba); }
unsigned long long gb_to_lba(unsigned long long size) { return GB_TO_LBA(size); }
unsigned long long mb_to_lba(unsigned long long size) { return MB_TO_LBA(size); }
unsigned long long kb_to_lba(unsigned long long size) { return KB_TO_LBA(size); }
unsigned long long bt_to_lba(unsigned long long size) { return BT_TO_LBA(size); }

unsigned long long (*lba_to_op[4]) (unsigned long long lba) = {
    lba_to_gb, lba_to_mb, lba_to_kb, lba_to_bt };

unsigned long long (*size_to_op[4]) (unsigned long long density) = {
    gb_to_lba, mb_to_lba, kb_to_lba, bt_to_lba };

unsigned long long convert_size_to_lba(char *size_ptr, SIZE_MODE SIZE)
{
    unsigned long long new_size;

    new_size = strtoull(size_ptr, (char**)NULL, 10);

    return (*size_to_op[SIZE]) (new_size);
}

unsigned long long convert_lba_to_size(char *lba_ptr, SIZE_MODE SIZE)
{
    unsigned long long new_lba;

    if (strstr(lba_ptr,"0x"))
        new_lba = strtoull(lba_ptr, (char**)NULL, 16);
    else
        new_lba = strtoull(lba_ptr, (char**)NULL, 10);

    return (*lba_to_op[SIZE]) (new_lba);
}

void print_out(unsigned long long output, char* out_ptr)
{
    if (out_ptr != NULL && strcmp(out_ptr,"hex") == 0)
        printf("0x%llx\n", output);
    else
        printf("%llu\n", output);
}

SIZE_MODE determine_size_mode(char *str_ptr)
{
    SIZE_MODE SIZE;
    if (strstr(str_ptr,"g"))
        SIZE = GB_MODE;
    else if (strstr(str_ptr,"m"))
        SIZE = MB_MODE;
    else if (strstr(str_ptr,"k"))
        SIZE = KB_MODE;
    else if (strstr(str_ptr,"b"))
        SIZE = BYTE_MODE;

    return SIZE;
}

void help(void)
{
    printf("Usage: lbaconversioner MODE [OPTION]...\n\n");
    printf("%-8s | %s %s", "--lba", "-l", "Specify the specifc logical block address to convert\n");
    printf("%-8s | %s %s", "--size", "-s", "Specify the specific density or size of device. Requires size in format NUM[g,m,k,b], etc 1g for 1 gigabyte\n");
    printf("%-8s | %s %s", "--format", "-f", "Specify the unit format for lba mode. Defaults to gigabyte mode but excepts g,m,k,b\n");
    printf("%-8s | %s %s","--output", "-o", "Specify the final output format of conversioner. Supports dec(decimal) and hex(hexidecimal) formats\n");

    exit(EXIT_FAILURE);
}

int parse_args(int argc, char *argv[]) 
{
    int opt;
    char *mode_ptr = NULL;
    //char *size_ptr = NULL;
    char *unit_ptr = NULL;
    //char *lba_ptr = NULL;
    char *out_ptr = NULL;
    char *value_ptr = NULL;

    SIZE_MODE size_mode = -1;

    while  ((opt = getopt_long(argc, argv, "l:s:f:o:h", long_options, NULL)) != -1) {
        switch (opt) {
            case 'l':
                if (mode_ptr != NULL && value_ptr != NULL) {
                    printf("Error: Argument list may not contain both --lba and --size\n");
                    return EXIT_FAILURE;
                } 
                value_ptr = optarg;
                mode_ptr = "lba";
                break;
            case 's':
                if (mode_ptr != NULL && value_ptr != NULL) {
                    printf("Error: Argument list may not contain both --lba and --size\n");
                    return EXIT_FAILURE;
                } 
                value_ptr = optarg;
                mode_ptr = "size";
                size_mode = determine_size_mode(value_ptr);
                break;
            case 'f':
                unit_ptr = optarg;
                size_mode = determine_size_mode(unit_ptr);
                break;
            case 'o':
                out_ptr = optarg;
                break;
            case 'h':
                help();
                break;
            default:
                break;
        }
    }


    // Default size mode if we don't set it
    if (size_mode == -1) {
        size_mode = GB_MODE;
    }

    if (mode_ptr == NULL || value_ptr == NULL || unit_ptr == NULL)
        help();

    return conversioner(mode_ptr, value_ptr, unit_ptr, out_ptr, size_mode);    
}

int conversioner(char *mode_ptr, char *value_ptr, char *unit_ptr, char *out_ptr, SIZE_MODE size_mode)
{
    unsigned long long value;
    if (strcmp(mode_ptr,"lba") == 0) {
        value = convert_lba_to_size(value_ptr, size_mode);
    }
    else if (strcmp(mode_ptr,"size") == 0) {
        value = convert_size_to_lba(value_ptr, size_mode);
    }
    else {
        printf("Invalid mode provided\n");
        return EXIT_FAILURE;
    }

    if (value == 0) {
        printf("Inappropriate value returned during conversion\n");
        return EXIT_FAILURE;
    }

    print_out(value, out_ptr);
    return EXIT_SUCCESS;
}
