#ifndef CONVERSIONER_H
#define CONVERSIONER_H

#define LBA_TO_GB(x) ((x - 21168) / 1953504)
#define LBA_TO_MB(x) (LBA_TO_GB(x) * 1024) //((x - 21168) / 1953504) * 1024
#define LBA_TO_KB(x) (LBA_TO_MB(x) * 1024) //((x - 21168) / 1953504) * 1024 * 1024
#define LBA_TO_BT(x) (LBA_TO_KB(x) * 1024) //((x - 21168) / 1953504) * 1024 * 1024 * 1024

#define GB_TO_LBA(x) ((x * 1953504) + 21168)
#define MB_TO_LBA(x) (GB_TO_LBA(x / 1024))
#define KB_TO_LBA(x) (MB_TO_LBA(x / 1024))
#define BT_TO_LBA(x) (KB_TO_LBA(x / 1024))

#define GB_TO_LBA_4k_0(x) ((12212046) + (244188 * x - 50))

typedef enum
{
    GB_MODE = 0,
    MB_MODE,
    KB_MODE,
    BYTE_MODE
} SIZE_MODE;

unsigned long long lba_to_gb(unsigned long long lba);
unsigned long long lba_to_mb(unsigned long long lba);
unsigned long long lba_to_kb(unsigned long long lba);
unsigned long long lba_to_bt(unsigned long long lba);
unsigned long long gb_to_lba(unsigned long long size);
unsigned long long mb_to_lba(unsigned long long size);
unsigned long long kb_to_lba(unsigned long long size);
unsigned long long bt_to_lba(unsigned long long size);

unsigned long long (*lba_to_op[4]) (unsigned long long lba);
unsigned long long (*size_to_op[4]) (unsigned long long density);

unsigned long long convert_size_to_lba(char *size_ptr, SIZE_MODE SIZE);
unsigned long long convertlba_to_size(char *lba_ptr, SIZE_MODE SIZE);

void print_out(unsigned long long output, char* out_ptr);

SIZE_MODE determine_size_mode(char *str_ptr);

void help(void);

int parse_args(int argc, char *argv[]);

int conversioner(char *mode_ptr, char *value_ptr, char *unit_ptr, char *out_ptr, SIZE_MODE size_mode);

#endif // CONVERSIONER_H
